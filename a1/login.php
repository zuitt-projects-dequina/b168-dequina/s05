<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <style>
        body {
            border: 1px solid black;
            border-radius: 10px;
            margin-left: 10%;
            margin-right: 10%;
            margin-top: 10%;
            padding-left: 1rem;
            padding-right: 1rem;
            padding-bottom: 1rem;
            display: inline-block;
        }

        span {
            width: 80px;
            display: inline-block;
        }
    </style>
    <title>S05 Activity Code</title>
</head>

<body>
    <?php session_start() ?>

    <h3>Login</h3>

    <form method="POST" action="./server.php">
        <input type="hidden" name="action" value="login" />

        <span>Email: </span>

        <input type="email" name="email" placeholder="Email"><br>

        <span>Password: </span>

        <input type="password" name="password" placeholder="Password""><br>

        <button type="submit">Login</button>
    </form>

</body>

</html>