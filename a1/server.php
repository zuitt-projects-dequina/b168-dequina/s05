<?php

session_start();

class LoginPage {
   
    public function login($email, $password) {

        // create a User's Array
        $_SESSION['users'] = array();
        

        // adding user to users Array
        array_push($_SESSION['users'], $email);

        header('Location: ./welcome.php');
    }


    public function logout() {
        session_destroy();
        header('Location: ./login.php');
    }
}

$loginPage = new LoginPage();

// conditions
if (
    ($_POST['action'] === 'login') &&
    ($_POST['email'] === 'johnsmith@gmail.com') &&
    ($_POST['password'] === '1234')
    ) {
    $loginPage->login($_POST['email'], $_POST['password']);
    //echo ($_POST['email']);
}
else if ($_POST['action'] === 'logout') {
    $loginPage->logout();  
}

