<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <style>
        body {
            border: 1px solid black;
            border-radius: 10px;
            margin-left: 10%;
            margin-right: 10%;
            margin-top: 10%;
            padding-left: 1rem;
            padding-right: 1rem;
            padding-bottom: 1rem;
            display: inline-block;
        }
    </style>
    <title>S05 Activity Code</title>
</head>

<body>
    <?php session_start() ?>

    <form method="POST" action="./server.php">
        <input type="hidden" name="action" value="logout" />

        <p>Hello, <?php echo ($_SESSION['users'][0]); ?>!</p>

        <button type="submit">Logout</button>
    </form>

</body>

</html>