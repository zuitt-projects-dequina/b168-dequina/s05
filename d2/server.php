<?php

// A session is a way to store information (in variables) to be used across multiple pages. Unlike a cookie, the information is not stored on the users computer.

session_start();

class TaskList {
    public function add($description) {
        $newTask = (object)[
            'description' => $description,
            'isFinished' => false
        ];

        // create an Array even there is no Task created
        if ($_SESSION['tasks'] === null) {
            $_SESSION['tasks'] = array();
        }

        // adding the task on the new array
        array_push($_SESSION['tasks'], $newTask);
    }

    public function update($id, $description, $isFinished) {
        $_SESSION['tasks'][$id]->description = $description;
        $_SESSION['tasks'][$id]->isFinished = ($isFinished !== null) ? true : false;
    }

    public function remove($id) {
        // array_splice(array, start, length)
        array_splice($_SESSION['tasks'], $id, 1);
    }

    public function clear() {
        session_destroy();
    }
}

$taskList = new TaskList();

// to add the task if the value='add/update/remove' in index.php match the this if statment
if ($_POST['action'] === 'add') {
    $taskList->add($_POST['description']);
}
else if ($_POST['action'] === 'update') {
    $taskList->update($_POST['id'], $_POST['description'], $_POST['isFinished']);
}
else if ($_POST['action'] === 'remove') {
    $taskList->remove($_POST['id']);
}
else if ($_POST['action'] === 'clear') {
    $taskList->clear();
}

header('Location: ./index.php');