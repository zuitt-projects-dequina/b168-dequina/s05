<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <style>
        body {
            border: 1px solid black;
            border-radius: 10px;
            margin-left: 10%;
            margin-right: 10%;
            margin-top: 10%;
            padding: 1rem;
        }
    </style>
    <title>S05: Client-Server Communication (Basic To-Do)</title>
</head>
<body>
    <!-- initializes session data -->
    <!-- creates a session or resumes the current one based on a session identifier passed via a GET or POST request, or passed via a cookie. When session_start() is called or when a session auto starts, PHP will call the open and read session save handlers -->
    <?php session_start() ?>

    <h3>Add Task</h3>
    <form method="POST" action="./server.php">
        <input type="hidden" name="action" value="add" />
        Description: <input type="text" name="description" required>
        <button type="submit">Add</button>
    </form>
    
    <br/><hr/>

    <h3>Task List</h3>

    <?php if (isset($_SESSION['tasks'])): ?>
        <!-- The "as" keyword is used by the foreach loop to establish which variables contain the key and value of an element -->
        <?php foreach($_SESSION['tasks'] as $index => $task): ?>
        <div>
            <!-- style inline block is used so that elements display side by side -->
            <form method="POST" action="./server.php" style="display: inline-block">

                <input type="hidden" name="action" value="update">
                <input type="hidden" name="id" value="<?php echo $index; ?>">
                <input type="checkbox" name="isFinished" <?php echo ($tasks->isFinished) ? 'checked' : null ?>>
                <input type="text" name="description" value="<?php echo $task->description; ?>" style="border: none">
                <input type="submit" value="Update">
            </form>

            <form method="POST" action="./server.php" style="display: inline-block">
                <input type="hidden" name="action" value="remove">
                <input type="hidden" name="id" value="<?php echo $index; ?>">
                <input type="submit" value="Delete">
            </form>
        </div>
        <!-- endforeach; is used to terminate the if statement made above -->
        <?php endforeach; ?>
        <!-- endif; is used to terminate the if statement made above -->
    <?php endif; ?>

    <br><hr/>
    <form method="POST" action="./server.php">
        <input type="hidden" name="action" value="clear">
        <button type="submit">Clear All Tasks</button>
    </form>
</body>
</html>